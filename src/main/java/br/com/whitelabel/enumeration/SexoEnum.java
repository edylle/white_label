package br.com.whitelabel.enumeration;

/**
 *
 * @author edylle
 */
public enum SexoEnum {

    MASCULINO("Masculino"),
    FEMININO("Feminino");

    private final String descricao;

    private SexoEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getLabel() {
        return descricao;
    }
}
