package br.com.whitelabel.testes;

import br.com.whitelabel.dao.EstabelecimentoDAO;
import br.com.whitelabel.entity.Estabelecimento;

/**
 *
 * @author edylle
 */
public class TesteCadastroEstabelecimento {

    public static void main(String[] args) {
        Estabelecimento e = new Estabelecimento();
        EstabelecimentoDAO dao = new EstabelecimentoDAO();
        
        e.setNome("Nome Etabelecimento");
        
        dao.startOperation();
        dao.save(e);
        dao.stopOperation(true);
    }

}
